from keras.models import load_model
import librosa
import tensorflow as tf
import numpy as np

SAVED_MODEL_PATH = "models/1D_CNNSounds_weights.h5"
SAMPLES_TO_CONSIDER = 16000


model_loaded = load_model(SAVED_MODEL_PATH)

class _Sound_Classification:

    model = None
    _mapping = [
        "Warning",
        "Normal"
    ]

    _instance = None


    def predict(self, file_path):

        # extract MFCC
        MFCCs,signal = self.preprocess(file_path)

        # we need a 4-dim array to feed to the model for prediction: (# samples, # coefficients, 1)
        MFCCs = MFCCs[np.newaxis,...]
        print("shape:MFCCs",MFCCs.shape)
        
        #get the predicted label
        predictions = model_loaded.predict(MFCCs)
        predicted_index = np.argmax(predictions)
        predicted_keyword = self._mapping[predicted_index]
        print("predicted_keyword",predicted_keyword)

        return (predicted_keyword,signal)



    def preprocess(self, file_path, num_mfcc=40):

        # load audio file
        signal, sample_rate = librosa.load(file_path)

        if len(signal) >= SAMPLES_TO_CONSIDER:
            # ensure consistency of the length of the signal
            signal = signal[:SAMPLES_TO_CONSIDER*5]

        # extract MFCCs
        MFCCs = np.mean(librosa.feature.mfcc(signal, sample_rate, n_mfcc=num_mfcc).T,axis=0) 
        MFCCs = np.array(MFCCs).reshape([-1,1])
        print("shape:MFCCs",MFCCs.shape)
        return (MFCCs,signal)



def Sound_Classification():
    # ensure an instance is created only the first time the factory function is called
    if _Sound_Classification._instance is None:
        _Sound_Classification._instance = _Sound_Classification()
        _Sound_Classification.model = tf.keras.models.load_model(SAVED_MODEL_PATH)
    return _Sound_Classification._instance



if __name__ == "__main__":

    # create 2 instances of the Sound_Classification
    kss = Sound_Classification()
    kss1 = Sound_Classification()

    # check that different instances of the Sound_Classification point back to the same object (singleton)
    assert kss is kss1
