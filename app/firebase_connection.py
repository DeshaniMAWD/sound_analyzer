import firebase_admin   
from firebase_admin import credentials
from firebase_admin import firestore
import json
from datetime import datetime

JSON_PATH = "data.json"


#firebase-firestore upload
cred = credentials.Certificate('key.json')
firebase_admin.initialize_app(cred)
db = firestore.client()
doc_ref =db.collection('sound')

class _FirebaseConnction:
    
    _instance = None

    def uploadfirestore(self,inditify):
    
        #set time to genarate ID
        uniq_id=datetime.now().strftime('%Y%m%d%H%M%S')
        
        if  inditify=='sound'  : 
            #load json file-heart
            with open(JSON_PATH) as file:
                data01=json.load(file)
            print(data01["result"])

            try:
                doc_ref.document(uniq_id).set({'sound':data01["result"]})
                print("sent to firebase")
                
            except Exception as e:
                print("error")

 
            
def FirebaseConnction():
    # ensure an instance is created only the first time the factory function is called
    if _FirebaseConnction._instance is None:
        _FirebaseConnction._instance = _FirebaseConnction()
    return _FirebaseConnction._instance



