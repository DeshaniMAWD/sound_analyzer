from re import I
import random
import os
import pandas as pd
import numpy as np
from flask import Flask, flash, request, redirect, render_template
import json
import plotly.figure_factory as ff
import configparser
import speech_recognition as sr




from firebase_connection import FirebaseConnction
from sound import Sound_Classification



JSON_PATH = "data.json"

r = sr.Recognizer()

app = Flask(__name__,static_url_path='/static')
app.secret_key = os.urandom(24)

ALLOWED_EXTENSIONS = set(['wav', 'mp3'])
#print(os.getcwd())
def get_default_config():
    conf = configparser.ConfigParser()
    conf.read('../conf/config.ini')
    config = conf['DEFAULT']
    return config


conf = get_default_config()
deploy_type = conf['deploy_type']
print(deploy_type)

hostname = conf['hostname']
port = conf['port']


@app.route('/')
def upload_form():

    return render_template('index.html',hostname=hostname, port=port)

def allowed_file(filename):

	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def Conversion(file, lang):
    all_=[]
    with sr.AudioFile(file) as source:
        print("Fetching file")
        audio_text = r.listen(source)
        try:
            print("converting audio to text....")
            text = r.recognize_google(audio_text, language=lang)
            all_.append(text)
        except:
            print("sorry,run again...")
    print(all_)
            

    

@app.route('/uploader', methods=['GET','POST'])
def uploader():

	if request.method == 'POST':

		# get file from POST request and save it
		test_flag = ''
		if 'file' in request.files:
			test_flag = 'file'
		
		audio_file = request.files['file']
        
        
        
        # Conversion(audio_file,"si-LK")
		if audio_file.filename == '':
				flash('No file selected for uploading')
				return redirect(request.url)
		if audio_file and allowed_file(audio_file.filename):
                    file_name = str(random.randint(0,100000))
                    audio_file.save(file_name)
                    print("audio")
                    print(audio_file.filename)
                    print("outputtt")
                    all_=[]
                    new=str(audio_file.filename)
                    print(new)
                    with sr.AudioFile(new) as source:
                        print("Fetching file")
                        audio_text = r.listen(source)
                        try:
                            print("converting audio to text....")
                            lang="si-LK"
                            text = r.recognize_google(audio_text, language=lang)
                            all_.append(text)
                        except:
                            print("sorry,run again...")
                    print(all_)
                    
                
                    kss = Sound_Classification()
                    predicted_keyword,signal = kss.predict(file_name)
                    os.remove(file_name)
                    data_audio = {
        				"result": [],
        				"signal": []
        				}
                    data_audio["result"].append(predicted_keyword)
                    data_audio["signal"].append(signal.T.tolist())
                    with open(JSON_PATH, "w") as fp:
                        json.dump(data_audio, fp, indent=4)
                    out_df1=pd.DataFrame(columns =['Sound_Type'],data =np.array([predicted_keyword]).transpose())
                    out_df1['User_id'] = out_df1.index+1
                    out_df1 = out_df1[['User_id', 'Sound_Type']]
                    colorscale = [[0, '#00474d'],[.5, '#057fa8'],[1, '#faf5f5']]
                    table = ff.create_table(out_df1, colorscale=colorscale, height_constant = 20)
                    table.to_html()
                    pp_table = table.to_html()
                    fc = FirebaseConnction()
                    fc.uploadfirestore('sound')
                    return render_template('response.html',table = pp_table,text=all_)
                

			
			# instantiate keyword spotting service singleton and get prediction
#     			kss = Sound_Classification()
#     			predicted_keyword,signal = kss.predict(file_name)

# 			# we don't need the audio file any more - let's delete it!
# 			os.remove(file_name)

# 			# dictionary where we'll store mapping, labels, MFCCs and filenames
# 			data_audio = {
# 				"result": [],
# 				"signal": []
# 				}

# 			data_audio["result"].append(predicted_keyword) 
# 			data_audio["signal"].append(signal.T.tolist()) 

			# save data in json file
# 			with open(JSON_PATH, "w") as fp:
# 		   		json.dump(data_audio, fp, indent=4)

# 			#create table
# 			out_df1=pd.DataFrame(columns =['Sound_Type'],data =np.array([predicted_keyword]).transpose())
# 			out_df1['User_id'] = out_df1.index+1
# 			out_df1 = out_df1[['User_id', 'Sound_Type']]

# 			colorscale = [[0, '#00474d'],[.5, '#057fa8'],[1, '#faf5f5']]
# 			table = ff.create_table(out_df1, colorscale=colorscale, height_constant = 20)
# 			table.to_html()
# 			pp_table = table.to_html()

# 			#Firebase upload
# 			fc = FirebaseConnction()
# 			fc.uploadfirestore('sound')
# 	

# 			return render_template('response.html',table = pp_table)
		else:
				flash('Allowed file types are wav,mp3')
				return redirect(request.url)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=port,threaded=False)