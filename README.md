# Sound Analyzer

This system provides you the ability of identifying a criminal situation according to uploaded recordings using voice recognition.

## Requirements

#### Flask==2.0.1
#### pandas==1.2.4
#### numpy==1.19.5
#### plotly==5.1.0
#### tensorflow==2.5.0
#### librosa==0.8.0
#### Keras==2.4.3
#### firebase-admin==5.2.0 
